use wasm_minimal_protocol::*;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use std::io::Write;

initiate_protocol!();

#[wasm_func]
pub fn compress_zlib(input_data: &[u8]) -> Vec<u8> {
    // Compress the input data using zlib
    let mut encoder = ZlibEncoder::new(Vec::new(), Compression::default());
    encoder.write_all(input_data).unwrap();
    encoder.finish().unwrap()
}
